FROM eclipse-temurin:19-jdk

LABEL mentainer = "buzz83sg76@gmail.com"

WORKDIR /app

copy target/springboot-docker-demo-0.0.1-SNAPSHOT.jar /app/springboot-docker-demo.jar

ENTRYPOINT ["java", "-jar", "springboot-docker-demo.jar"]